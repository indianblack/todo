![Project image](public/todo-img.png)

# To-do app

Simple to-do app using Vue.js

## Basic requirements
User should be able to:
- create a list of tasks,
- mark tasks as done,
- remove tasks from list.

The application should be a web page formatted in CSS. Optionally, use a JS framework.

## Usage
1. Use the input field on the top to add new to-do item. Add description and click 'Enter' to save.
2. Doubleclick on a to-do item to edit it. Then click 'Enter' to save changes or 'Esc' to dismiss changes.
4. Click the 'X' button on the right side to remove an item.
5. Check the checkbox on the left to mark the item as complete.
6. Use the 'Check all' checkbox to to mark all items as complete.
7. Use filter buttons at the bottom to filter the to-do list.

## Instructions

**Basic**
1. Clone this repository.
2. Open project in code editor.
4. Setup project
```
npm install
```
5. Compile and hot-reload for development
```
npm run serve
```
6. Open in browser 
```
- Local:   http://localhost:8080/ 
- Network: http://10.0.0.113:8080/
```

**Other**

- Compile and minifie for production
```
npm run build
```

- Lint and fix files
```
npm run lint
```

- Customize configuration - see [configuration Reference](https://cli.vuejs.org/config/).

## Application screenshot

![Application screenshot](public/screenshot.png)

## Useful links
- Vue.js Todo app: https://www.youtube.com/watch?v=A5S23KS_-bU
- Vue documentation: https://vuejs.org/
- Adding Sass to vue: https://vue-loader.vuejs.org/guide/pre-processors.html
- Vetur can't find tsconfig.json, jsconfig.json in /xxxx/xxxxxx: https://vuejs.github.io/vetur/guide/FAQ.html#vetur-uses-different-version-of-typescript-in-vue-files-to-what-i-installed-in-node-modules
- Vue panel not showing in devtools: https://stackoverflow.com/questions/41505150/vuejs-dev-tools-panel-not-showing
- Animate.css: https://animate.style/
- Vuejs Animate.css not working: https://stackoverflow.com/questions/44794501/vuejs-animate-css-not-working
- Illustrations: https://icons8.com/illustrations
